package com.javastudy.abstractinterface;

public class MacKeyboard
        extends AbstractKeyboard
        implements Keyboard{
    @Override
    public String printMessage() {
        System.out.println("Printing on MAC.");
        return "MAC";
    }

    @Override
    public void printSomethingAbstract() {
        System.out.println("Printing abstract MAC.");
    }
}

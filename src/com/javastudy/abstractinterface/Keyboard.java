package com.javastudy.abstractinterface;

public interface Keyboard{
    String printMessage();
    default void aliveKeyboard(){
        System.out.println("Keyboard live.");
    }


}

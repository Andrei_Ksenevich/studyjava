package com.javastudy.abstractinterface;

public class WindowsKeyboard
    extends AbstractKeyboard
        implements Keyboard, SensorPanel {
    @Override
    public String printMessage() {
        System.out.println("Printing on Windows.");
        return "Windows";
    }

    @Override
    public String swipe() {
        System.out.println("Swipe on windows");
        return "Swipe Windows";
    }

    @Override
    public void printSomethingAbstract() {
        System.out.println("Printing abstract WIN.");
    }
}

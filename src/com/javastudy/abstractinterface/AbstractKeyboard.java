package com.javastudy.abstractinterface;

public abstract class AbstractKeyboard {

    public void printSomething(){
        System.out.println("Print Something From Abstract.");
    }

    public abstract void printSomethingAbstract();
}

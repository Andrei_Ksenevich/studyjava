package com.javastudy.abstractinterface;

public interface SensorPanel {
    String swipe();
    default void alivePanel(){
        System.out.println("SensorPanel live.");
    }
}

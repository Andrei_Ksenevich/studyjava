package com.javastudy.exept;

public class ExceptionHandlingCustom {

    public static void main(String args[]){

        try {
            throw new MyException();

        }catch (MyException e1){
            System.out.println("My exept");
        }

        catch (Exception e) {
            System.out.println("General exept");
        }
    }

    public static class MyException extends  Exception{}

}

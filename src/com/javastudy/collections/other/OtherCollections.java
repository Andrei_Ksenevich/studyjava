package com.javastudy.collections.other;

import java.util.*;

import static javafx.scene.input.KeyCode.Q;

public class OtherCollections {
    public static void main(String args[]){
        Vector vector = new Vector<>();
        vector.add("0");
        vector.add("1");
        vector.add("2");
        System.out.println(vector);
        Stack stack = new Stack();
        stack.push("0");
        stack.push("1");
        stack.push("2");
        System.out.println(stack.peek());
        Queue queue = new PriorityQueue();
        queue.offer("0");
        queue.offer("0");
        queue.offer("1");
        System.out.println(queue);
        }
    }

package com.javastudy.collections.linkedlist;

import java.util.ArrayList;
import java.util.LinkedList;

public class LearnLinkedList {

    public static void main(String[] args) {
        // write your code here
        System.out.println("Build ok.");
        LinkedList linkedList = new LinkedList();
        System.out.println(linkedList.size());
        linkedList.add("A");
        System.out.println(linkedList.size());
        System.out.println(linkedList.getFirst());
        System.out.println(linkedList.getLast());
        linkedList.addLast("B1");
        linkedList.add("B");
        linkedList.offer("B1");
        System.out.println(linkedList.toString());
        linkedList.remove("B1");
        linkedList.addFirst("A0");
        linkedList.remove(1);
        System.out.println(linkedList.toString());
        System.out.println(linkedList.iterator().next());
    }
}

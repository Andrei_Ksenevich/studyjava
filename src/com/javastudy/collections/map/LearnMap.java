package com.javastudy.collections.map;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LearnMap {
    public static void main(String args[]) {
        HashMap hashMap = new HashMap();
        hashMap.put("2", "two");
        hashMap.put("0", "zero");
        hashMap.put("1", "one");
        hashMap.put("0", "not zero");
        hashMap.put("0", "zero");
        System.out.println(hashMap);
        System.out.println(hashMap);
        System.out.println(hashMap.entrySet());
        System.out.println(hashMap.keySet());
        System.out.println(hashMap.values());
        for (Object key : hashMap.keySet())
            System.out.println(hashMap.get(key));

        System.out.println("LINKED");
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        linkedHashMap.put("2", "two");
        linkedHashMap.put("0", "zero");
        linkedHashMap.put("1", "one");
        linkedHashMap.put("0", "not zero");
        linkedHashMap.put("0", "zero");
        System.out.println(linkedHashMap);
        System.out.println(linkedHashMap);
        System.out.println(linkedHashMap.entrySet());
        System.out.println(linkedHashMap.keySet());
        System.out.println(linkedHashMap.values());
        for (Object key : linkedHashMap.keySet())
            System.out.println(linkedHashMap.get(key));
    }
}

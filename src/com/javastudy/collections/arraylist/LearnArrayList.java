package com.javastudy.collections.arraylist;

import java.util.ArrayList;
import java.util.TreeMap;

public class LearnArrayList {

    public static void main(String[] args) {
        // write your code here
        System.out.println("Build ok.");
        ArrayList arrayList = new ArrayList();
        System.out.println(arrayList.size());
        arrayList.add("A");
        System.out.println(arrayList.size());
        arrayList.add("B1");
        arrayList.add("B");
        arrayList.add("B1");
        System.out.println(arrayList.toString());
        arrayList.remove("B1");
        arrayList.add(0,"A0");
        arrayList.remove(1);
        System.out.println(arrayList.toString());
        System.out.println(arrayList.iterator().next());
    }
}

package com.javastudy.collections.set;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class LearnSet {
    public static void main(String[] args) {
        // write your code here
        System.out.println("Build ok.");
        HashSet hashSet = new HashSet();
        hashSet.add("0");
        hashSet.add("0");
        hashSet.add("2");
        hashSet.add("1");
        System.out.println(hashSet);
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.add("0");
        linkedHashSet.add("2");
        linkedHashSet.add("1");
        linkedHashSet.add("0");
        System.out.println(linkedHashSet);
        TreeSet treeSet = new TreeSet();
        treeSet.add("0");
        treeSet.add("2");
        treeSet.add("1");
        treeSet.add("0");
        System.out.println(treeSet);

    }
}

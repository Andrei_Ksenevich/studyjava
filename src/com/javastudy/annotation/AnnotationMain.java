package com.javastudy.annotation;

import java.lang.annotation.*;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class AnnotationMain {
    public static void main(String args[]) throws Exception {

        Cat cat = new Cat("Kitty", 10, "Black");
        Field[] fields = Cat.class.getDeclaredFields();
        Annotation[] annotation = cat.getClass().getDeclaredAnnotations();
        System.out.println(fields[0]);
        System.out.println(fields[0].getType());
        fields[0].setAccessible(true);
        System.out.println( fields[0].get(cat));
        fields[0].set(cat,"Boris");
        System.out.println( fields[0].get(cat));
        //System.out.println(cat.name);
        System.out.println(fields[0].getDeclaredAnnotations()[0]);
        ShowField sf = fields[0].getAnnotation(ShowField.class);
        System.out.println(sf.number());

        System.out.println("Reflection:");
        System.out.println(cat.getClass().getName()+" "+ cat.getClass().getSimpleName());
        System.out.println(cat.getClass().isEnum()+" "+ cat.getClass().isAnnotation());
        System.out.println(cat.getClass().getDeclaredFields()[0]);
        System.out.println(cat.getClass().getDeclaredFields()[0].getType());
        Field nameField = Cat.class.getDeclaredField("name");
        nameField.setAccessible(true);
        nameField.set(cat,"Kitty2");
        System.out.println(nameField.get(cat));
        Method method = Cat.class.getDeclaredMethod("meow", null);
        method.invoke(cat, null);
        Cat cat1 = Cat.class.newInstance();
        cat1 = Cat.class.getConstructor(null).newInstance();
        cat1.meow();
    }
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface ShowField{
boolean value() default true;
int number();
}

class Cat{
    @ShowField(number=1)
    private String name;
    @ShowField(value = false,number=2)
    int weight;
    @ShowField(number=3)
    String color;

    public Cat(){
    }

    public Cat(String name, int weight, String color){
        this.name = name;
        this.weight = weight;
        this.color = color;
    }

    public void meow(){
        System.out.println("Meow from reflection");
    }


}
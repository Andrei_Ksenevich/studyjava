package com.javastudy.Serialization;

import java.io.Serializable;

public class Student implements Serializable{
    private static final String status="Student";
    private String name;
    private String number;
    private int score;
    private transient int id;

    public Student(String name, String number, int score, int id) {
        this.name = name;
        this.number = number;
        this.score = score;
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return status+";"+this.name+";"+this.number+";"+this.id+";"+this.score;
    }
}

package com.javastudy.methodsandfunctions;

import java.util.Arrays;

public class MethodsLearn {

    public static void main (String [] args) {
       long startTime=System.nanoTime();
       int[] array = {9,7,1,0,2,3,4,5,6,10,7,8};
       System.out.println(Arrays.toString(array));
       array = bubbleSort(array);
       System.out.println(Arrays.toString(array));
       System.out.println(System.nanoTime()-startTime);
    }

    public static int[] bubbleSort(int[] array){
        int swap;
        for(int i=0; i < array.length-1;i++){
            for(int j=0; j<array.length-i-1;j++){
                if(array[j]>array[j+1]){
                    swap=array[j];
                    array[j]=array[j+1];
                    array[j+1]=swap;
                }
            }
        }
        return array;
    }
}

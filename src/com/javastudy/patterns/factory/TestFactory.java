package com.javastudy.patterns.factory;

public class TestFactory{

    public static void main(String[] args){
        VechicleFactory vechicleFactory = new VechicleFactory();
        Vechicle vechicle1 = vechicleFactory.getVechicle("bycicle");
        Vechicle vechicle2 = vechicleFactory.getVechicle("motocycle");
        Vechicle vechicle3 = vechicleFactory.getVechicle("car");
        vechicle1.printVechicleType();
        vechicle2.printVechicleType();
        vechicle3.printVechicleType();

}}


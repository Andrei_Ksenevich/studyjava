package com.javastudy.patterns.factory;

import com.sun.org.apache.bcel.internal.generic.LADD;

public class TestAbstractFactory {

    public static void main(String[] args) {
        Automobile lada = new AbstractFactory().createFectory("Car").createAutomobile("LADA");
        Automobile audi = new AbstractFactory().createFectory("Car").createAutomobile("AUDI");
        Tank t34 = new AbstractFactory().createFectory("Tank").createTank("T34");
        Tank leopard = new AbstractFactory().createFectory("Tank").createTank("LEOPARD");
        lada.driveAuto();
        audi.driveAuto();
        t34.driveTank();
        leopard.driveTank();

    }


}

interface Automobile{
    void driveAuto();
}

class Lada implements Automobile{
    @Override
    public void driveAuto() {
        System.out.println("Driving LADA");
    }
}


class Audi implements Automobile{
@Override
public void driveAuto() {
        System.out.println("Driving AIDI");
}
}

interface Tank{
    void driveTank();
}

class T34 implements Tank{
    @Override
    public void driveTank() {
        System.out.println("Driving T34");
    }
}


class Leopard implements Tank{
    @Override
    public void driveTank() {
        System.out.println("Driving LEOPARD");
    }
}

interface Factory{
    Automobile createAutomobile(String typeOfCar);
    Tank createTank(String typeOfTank);

};

class AutoFacory implements Factory{
    public Automobile createAutomobile(String typeOfCar){
        switch(typeOfCar){
            case "LADA":
                return new Lada();
            case "AUDI":
                return new Audi();
            default: return null;

        }
    }

    @Override
    public Tank createTank(String typeOfTank) {
        return null;
    }
}

class TankFacory implements Factory{
    @Override
    public Automobile createAutomobile(String typeOfCar) {
        return null;
    }

    public Tank createTank(String typeOfTank){
        switch(typeOfTank){
            case "T34":
                return new T34();
            case "LEOPARD":
                return new Leopard();
            default: return null;

        }
    }
}

class AbstractFactory{
    Factory createFectory(String typeOfFactory){
        switch (typeOfFactory){
            case "Car": return new AutoFacory();
            case "Tank": return new TankFacory();
            default: return null;
        }
    }
}

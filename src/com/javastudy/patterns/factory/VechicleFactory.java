package com.javastudy.patterns.factory;

public class VechicleFactory {

    public Vechicle getVechicle(String vechicleType){
        if(vechicleType.equalsIgnoreCase("BYCICLE")){
            return new Bycicle();
        }
        if(vechicleType.equalsIgnoreCase("MOTOCYCLE")){
            return new Motocycle();
        }
        if(vechicleType.equalsIgnoreCase("CAR")){
            return new Car();
        }

        return null;
    }
}

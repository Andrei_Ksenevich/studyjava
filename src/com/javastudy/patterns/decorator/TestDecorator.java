package com.javastudy.patterns.decorator;

public class TestDecorator {
    public static void main(String args[]){
        FileStream fp = new FileBufferedReader(new FileSecureReader(new FileStreamReader()));
        fp.read();
        fp.read();
    }


}

interface FileStream {
    void read();
}

class FileStreamReader implements FileStream {

    @Override
    public void read() {
        System.out.println("Reading file.");
    }
}

abstract class FileDecorator implements FileStream {
    FileStream fileStream;
    public FileDecorator(FileStream fileStream){
        this.fileStream = fileStream;
    }

}

class FileBufferedReader extends FileDecorator {
    public FileBufferedReader(FileStream fileDecorator) {
        super(fileDecorator);
    }

    @Override
    public void read() {
        fileStream.read();
        System.out.println("Reading file in buffer.");

    }
}
    class FileSecureReader extends FileDecorator {
        public FileSecureReader(FileStream fileDecorator) {
            super(fileDecorator);
        }

        @Override
        public void read() {
            fileStream.read();
            System.out.println("Reading file Secure.");

        }
}
package com.javastudy.patterns.singleton;

public class TestSingleton {

    public static void main(String[] args){
        Singleton sg = Singleton.getInstance();
        sg.printMessage();
        sg.changeValue(0);
        sg.printMessage();
        Singleton sg1 = Singleton.getInstance();
        sg1.printMessage();
    }
}

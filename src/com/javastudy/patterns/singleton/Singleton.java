package com.javastudy.patterns.singleton;

public class Singleton {
    private int innerValue = 1;
    private static Singleton instance = new Singleton();
    private Singleton(){}
    public static Singleton getInstance(){
        return instance;
    }

    public void printMessage(){
        System.out.println("This is single object with value " + innerValue);
    }

    public void changeValue(int innerValue){
        this.innerValue = innerValue;
    }
}

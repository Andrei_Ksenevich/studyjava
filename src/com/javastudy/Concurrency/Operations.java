package com.javastudy.Concurrency;

public class Operations {

    public static void main(String args[]){
        final Account a = new Account(1000);
        final Account b = new Account(2000);

        new Thread(new Runnable() {
            @Override
            public void run(){
                transfer(a,b,500);
            }
        }).run();

        transfer(b,a,300);
        System.out.println("END");
    }

    static void transfer(Account ac1, Account ac2, int amount) {
        if (ac1.getBalance() < amount)
            System.out.println("MINUS");

        ac1.withdraw(amount);
        ac2.deposit(amount);
    }
}

package com.javastudy.generics;

public class Box<T,F> {
    private T t;
    private F f;

    public void add(T t, F f) {
        this.t = t;
        this.f = f;
    }

    public T getT() {
        return t;
    }

    public F getF() {
        return f;
    }
}

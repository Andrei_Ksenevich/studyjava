package com.javastudy.generics;

import com.javastudy.abstractinterface.AbstractKeyboard;
import com.javastudy.abstractinterface.MacKeyboard;
import com.javastudy.abstractinterface.WindowsKeyboard;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LearnGenerics {
    public static void main(String[] args) {
        Box<Integer, Double> integerBox = new Box<Integer, Double>();
        Box<String, Double> stringBox = new Box<>();
        integerBox.add(new Integer(10), Double.valueOf(11.5));
        stringBox.add("Hello World", Double.valueOf(1986.5));
        System.out.println("Integer Value : " + integerBox.getT());
        System.out.println("String Value : " + stringBox.getT());
        System.out.println("DoubleValue : " + integerBox.getF());
        System.out.println("Double Value : " + stringBox.getF());

        Integer[] intArray = { 1, 2, 3, 4, 5 };
        Double[] doubleArray = { 1.1, 2.2, 3.3, 4.4 };
        Character[] charArray = { 'H', 'E', 'L', 'L', 'O' };

        System.out.println("Array integerArray contains:");
        printArray(intArray);

        System.out.println("Array doubleArray contains:");
        printArray(doubleArray);

        System.out.println("Array characterArray contains:");
        printArray(charArray);

        List<Integer> integerList = Arrays.asList(1, 2, 3);
        System.out.println("sum = " + sum(integerList));

        List<Double> doubleList = Arrays.asList(1.2, 2.3, 3.5);
        System.out.println("sum = " + sum(doubleList));

        List<Number> numberList = Arrays.asList(1, 2, 3);
        printDouble(numberList);

        System.out.println(numberList.get(0).getClass());
        printAll(integerList);


        List<String> stringList = Arrays.asList("oneL", "twoL", "3L");
        String[] stringArray = {"oneAr", "twoAr", "3Ar"};
        System.out.println("MULTIPRINT Iterable.");
        printArrayAndList(stringList);
        System.out.println("MULTIPRINT [].");
        printArrayAndList(stringArray);

    }


    public static <L> void printArrayAndList(L arrayOrList){
        List<?> list = Arrays.asList(arrayOrList);
        for (Object element : list) {
            if (element instanceof Iterable) {
                for (Object objectInIterable : (Iterable) element) {
                    System.out.println(objectInIterable);
                }
            }
            if (element.getClass().isArray()) {
                for (int i = 0; i < Array.getLength(element); i++) {
                    System.out.println(Array.get(element, i));
                }
            }
        }
    }


    public static <E> void printArray(E[] array){
        for(E element: array){
            System.out.print(element +" ");
        }
        System.out.println("");
    }

    public static double sum(List<? extends Number> numberlist) {
        double sum = 0.0;
        for (Number n : numberlist) sum += n.doubleValue();
        return sum;
    }

    public static void printDouble(List<? super Double> list) {
        for (Object item : list)
            System.out.println(item + " ");
    }

    public static void printAll(List<?> list) {
        for (Object item : list)
            System.out.println(item + " ");
    }

}
